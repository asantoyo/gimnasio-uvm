SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

CREATE DATABASE IF NOT EXISTS `gym` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `gym`;

TRUNCATE TABLE `admin`;
INSERT INTO `admin` (`matricula`, `nombre`, `password`, `fechaRegistro`) VALUES
(110008003, 'Alex', '$2y$10$6DEqRTn5bP5QcmaIYudwE.tQpFOQxoEEw5EzLcejY7Q3mYL3iqGqO', '2015-05-03 20:40:35');

TRUNCATE TABLE `carrera`;
INSERT INTO `carrera` (`idCarrera`, `nombre`) VALUES
(1, 'Ingeniería en Mecatrónica'),
(2, 'Ingeniería en Sistemas Computacionales'),
(3, 'Ingeniería Industrial y de Sistemas'),
(4, 'Licenciatura en Administración de Empresas Turísticas'),
(5, 'Licenciatura en Administración de Negocios Internacionales'),
(6, 'Licenciatura en Arquitectura'),
(7, 'Licenciatura en Ciencias de la Comunicación'),
(8, 'Licenciatura en Cirujano Dentista'),
(9, 'Licenciatura en Comercio Internacional'),
(10, 'Licenciatura en Derecho'),
(11, 'Licenciatura en Fisioterapia'),
(12, 'Licenciatura en Medicina'),
(13, 'Licenciatura en Mercadotecnia'),
(14, 'Licenciatura en Nutrición'),
(15, 'Licenciatura en Psicología'),
(16, 'Licenciatura en Químico Farmacéutico Biotecnólogo'),
(17, 'Licenciatura en Relaciones Internacionales'),
(18, 'Licenciatura Internacional en Administración de Empresas de la Hospitalidad'),
(19, 'Licenciatura Internacional en Gastronomía'),
(20, 'Licenciatura en Administración de Empresas');

TRUNCATE TABLE `deporte`;
INSERT INTO `deporte` (`idDeporte`, `nombre`) VALUES
(1, 'Fútbol Americano'),
(2, 'Basquetbol'),
(3, 'Fútbol Rápido'),
(4, 'Fútbol Soccer'),
(5, 'Natación y clavados'),
(6, 'Tae kwon do'),
(7, 'Voleibol');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
