SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

CREATE DATABASE IF NOT EXISTS `gym` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
USE `gym`;

DROP TABLE IF EXISTS `admin`;
CREATE TABLE IF NOT EXISTS `admin` (
  `matricula` int(10) unsigned NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `fechaRegistro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `alumno`;
CREATE TABLE IF NOT EXISTS `alumno` (
  `matricula` int(10) unsigned NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `apellidos` varchar(50) NOT NULL,
  `idCarrera` int(10) unsigned NOT NULL,
  `idDeporte` int(10) unsigned DEFAULT NULL,
  `activo` tinyint(1) unsigned NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `carrera`;
CREATE TABLE IF NOT EXISTS `carrera` (
`idCarrera` int(10) unsigned NOT NULL,
  `nombre` varchar(80) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `certificado_medico`;
CREATE TABLE IF NOT EXISTS `certificado_medico` (
`idCertificado` int(10) unsigned NOT NULL,
  `matricula` int(10) unsigned NOT NULL,
  `fecha` date NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `deporte`;
CREATE TABLE IF NOT EXISTS `deporte` (
`idDeporte` int(10) unsigned NOT NULL,
  `nombre` varchar(20) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;

DROP TABLE IF EXISTS `registro_asistencia`;
CREATE TABLE IF NOT EXISTS `registro_asistencia` (
`idAsistencia` int(10) unsigned NOT NULL,
  `matricula` int(10) unsigned NOT NULL,
  `horaFecha` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;


ALTER TABLE `admin`
 ADD UNIQUE KEY `matricula` (`matricula`);

ALTER TABLE `alumno`
 ADD PRIMARY KEY (`matricula`), ADD KEY `idCarrera` (`idCarrera`), ADD KEY `idDeporte` (`idDeporte`);

ALTER TABLE `carrera`
 ADD PRIMARY KEY (`idCarrera`);

ALTER TABLE `certificado_medico`
 ADD PRIMARY KEY (`idCertificado`), ADD KEY `matricula` (`matricula`);

ALTER TABLE `deporte`
 ADD PRIMARY KEY (`idDeporte`);

ALTER TABLE `registro_asistencia`
 ADD PRIMARY KEY (`idAsistencia`), ADD KEY `matricula` (`matricula`);


ALTER TABLE `carrera`
MODIFY `idCarrera` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
ALTER TABLE `certificado_medico`
MODIFY `idCertificado` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
ALTER TABLE `deporte`
MODIFY `idDeporte` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;
ALTER TABLE `registro_asistencia`
MODIFY `idAsistencia` int(10) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=1;

ALTER TABLE `alumno`
ADD CONSTRAINT `fk_alumno_carrera` FOREIGN KEY (`idCarrera`) REFERENCES `carrera` (`idCarrera`) ON UPDATE CASCADE,
ADD CONSTRAINT `fk_alumno_deporte` FOREIGN KEY (`idDeporte`) REFERENCES `deporte` (`idDeporte`) ON DELETE SET NULL ON UPDATE CASCADE;

ALTER TABLE `certificado_medico`
ADD CONSTRAINT `fk_certificado_alumno` FOREIGN KEY (`matricula`) REFERENCES `alumno` (`matricula`) ON DELETE CASCADE ON UPDATE CASCADE;

ALTER TABLE `registro_asistencia`
ADD CONSTRAINT `fk_asistencia_alumno` FOREIGN KEY (`matricula`) REFERENCES `alumno` (`matricula`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
