Git repository:
=====================

The code for this project is hosted on Bitbucket
[Gym UVM](https://bitbucket.org/asantoyo/gimnasio-uvm/)

VHost configuration:
=====================

# GYM UVM
<VirtualHost *:80>
    ServerAdmin webmaster@gym.uvm.local
    ServerName gym.uvm.local
    DocumentRoot "/Users/jano/Documents/UVM/GYM/public"
</VirtualHost>

# GYM UVM SSL
<VirtualHost *:443>
    ServerName gym.uvm.local
    DocumentRoot "/Users/jano/Documents/UVM/GYM/public"
	
    SSLEngine on
    SSLCertificateFile  /Applications/XAMPP/xamppfiles/etc/ssl.crt/gym.uvm.crt
    SSLCertificateKeyFile /Applications/XAMPP/xamppfiles/etc/ssl.key/gym.uvm.key

    # And some extras, copied from Apache's default SSL conf virtualhost
    <FilesMatch "\.(cgi|shtml|phtml|php)$">
        SSLOptions +StdEnvVars
    </FilesMatch>

    BrowserMatch "MSIE [2-6]" \
        nokeepalive ssl-unclean-shutdown \
        downgrade-1.0 force-response-1.0
    # MSIE 7 and newer should be able to use keepalive
    BrowserMatch "MSIE [17-9]" ssl-unclean-shutdown
</VirtualHost>