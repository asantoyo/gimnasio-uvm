requirejs.config({
	paths: {
        jquery: 'jquery.min',
        'jquery-ui': 'jquery-ui.min',
		moment: 'moment.min',
		datatables: 'jquery.dataTables.min',
		'history': 'jquery-history',
		highcharts: 'highcharts/highcharts',
		'highcharts-more': 'highcharts/highcharts-more',
		'highcharts-3d': 'highcharts/highcharts-3d',
		'highcharts-exporting': 'highcharts/modules/exporting',
		'heatmap': 'highcharts/modules/heatmap',
		'datepicker' : 'bootstrap-datepicker/js/bootstrap-datepicker.min',
		'datepicker-es' : 'bootstrap-datepicker/locales/bootstrap-datepicker.es.min'
    },
	map: {
      '*': { 'jquery': 'jquery-private' },
      'jquery-private': { 'jquery': 'jquery' }
    },
	shim: {
		'moment': ['jquery'],
		'datatables': ['jquery'],
		'jquery-ui': ['jquery'],
		'highcharts': {
			deps: ['jquery'],
			exports: 'Highcharts'
		},
		'history' : {
			deps: ['jquery'],
			exports: 'History'
		},
		'highcharts-more': ['highcharts'],
		'highcharts-3d': ['highcharts'],
		'highcharts-exporting': ['highcharts'],
		'heatmap': ['highcharts']
	}
});
