<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application;

use Zend\Mvc\ModuleRouteListener;
use Zend\Mvc\MvcEvent;

use Zend\ModuleManager\Feature\ControllerPluginProviderInterface;
use Zend\ModuleManager\Feature\ViewHelperProviderInterface;
use Zend\ServiceManager\AbstractPluginManager;

use Zend\Authentication\AuthenticationService;
use Zend\Authentication\Storage\Session as SessionStorage;

use Spot\Config;
use Spot\Locator;

use Application\Form\Element\CarrerasSelect;
use Application\Form\Element\DeportesSelect;

use Application\Auth\Adapter;
use Application\Authorization\AuthListener;

use Application\Authorization\Acl;

class Module implements ControllerPluginProviderInterface, ViewHelperProviderInterface
{
    public function onBootstrap(MvcEvent $e)
    {
        $eventManager        = $e->getApplication()->getEventManager();
        $moduleRouteListener = new ModuleRouteListener();
        $moduleRouteListener->attach($eventManager);

		$app            = $e->getTarget();
		$serviceManager = $app->getServiceManager();
		$authListener   = $serviceManager->get('Application\Authorization\AuthListener');

		$serviceManager->get('viewhelpermanager')->setFactory('controllerName', function($sm) use ($e) {
			$viewHelper = new View\Helper\ControllerName($e->getRouteMatch());
			return $viewHelper;
		});

		$eventManager->attach($authListener);
    }

    public function getConfig()
    {
        return include __DIR__ . '/config/module.config.php';
    }

    public function getAutoloaderConfig()
    {
        return array(
            'Zend\Loader\ClassMapAutoloader' => array(
                 __DIR__ . '/autoload_classmap.php',
             ),
            'Zend\Loader\StandardAutoloader' => array(
                'namespaces' => array(
                    __NAMESPACE__ => __DIR__ . '/src/' . __NAMESPACE__,
                ),
            ),
        );
    }

	public function getServiceConfig()
     {
         return array(
             'factories' => array(
				 'Spot\Locator' =>  function($sm) {
					 $cfg = $sm->get('Spot\Config');
					 $spot = new Locator($cfg);
					 return $spot;
				 },
				 'Spot\Config' =>  function($sm) {
					 $options = $sm->get('Configuration');
					 $spotConn = $options['db'];
					 $cfg = new Config();
					 // MySQL
					 $cfg->addConnection('mysql', [
						 'dbname' => $spotConn['dbname'],
						 'user' => $spotConn['username'],
						 'password' => $spotConn['password'],
						 'host' => $spotConn['host'],
						 'driver' => 'pdo_mysql',
						 'charset' => 'utf8'
					 ]);
					 return $cfg;
				 },
				 'Application\Auth\Adapter' => function ($sm) {
					 return new Adapter();
				 },
				 'Zend\Authentication\AuthenticationService' => function ($sm) {
					 $auth = new AuthenticationService();
					 $auth->setStorage(new SessionStorage('UVM'));
					 return $auth;
				 },
				 'Application\Authorization\AuthListener' => function ($sm) {
					 return new AuthListener();
				 },
             )
         );
     }

	public function getViewHelperConfig()
    {
        return array(
            'factories' => array(
                'isAllowed' => function (AbstractPluginManager $pluginManager) {
                    $acl = Acl::getACL();
                    return new View\Helper\IsAllowed($acl);
                },
                'currentIdentity' => function (AbstractPluginManager $pluginManager) {
					$auth = $pluginManager->getServiceLocator()->get('Zend\Authentication\AuthenticationService');
					return new View\Helper\CurrentIdentity($auth);
                }
            ),
        );
    }

    public function getControllerPluginConfig()
    {
        return array(
            'factories' => array(
                'isAllowed' => function (AbstractPluginManager $pluginManager) {
                    $acl = Acl::getACL();
					return new Controller\Plugin\IsAllowed($acl);
                },
                'currentIdentity' => function (AbstractPluginManager $pluginManager) {
					$auth = $pluginManager->get('Zend\Authentication\AuthenticationService');
					return new Controller\Plugin\CurrentIdentity($auth);
                }
            ),
        );
    }

	public function getFormElementConfig()
    {
        return array(
            'factories' => array(
                'CarrerasSelect' => function($sm) {
					$spot = $sm->getServiceLocator()->get('Spot\Locator');
					$carreraMapper = $spot->mapper('Application\Model\Carrera');
                    $fieldset = new CarrerasSelect($carreraMapper);
                    return $fieldset;
                },
                'DeportesSelect' => function($sm) {
					$spot = $sm->getServiceLocator()->get('Spot\Locator');
					$carreraMapper = $spot->mapper('Application\Model\Deporte');
                    $fieldset = new DeportesSelect($carreraMapper);
                    return $fieldset;
                }
            )
        );
    }
}
