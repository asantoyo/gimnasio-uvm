<?php
namespace Application\Model;

use \Spot\MapperInterface as Mapper;
use \Spot\EntityInterface as Entity;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Alumno extends \Spot\Entity implements InputFilterAwareInterface
{
    protected static $table = 'alumno';
	protected $inputFilter;

    public static function fields()
    {
        return [
            'matricula'	=> ['type' => 'integer',	'primary' => true,	'unique' => true],
            'nombre'	=> ['type' => 'string',		'required' => true],
            'apellidos'	=> ['type' => 'string',		'required' => true],
            'idCarrera'	=> ['type' => 'integer',	'required' => true,	'index'	=>	true],
            'idDeporte'	=> ['type' => 'integer',	'index'	=>	true],
            'activo'	=> ['type' => 'boolean',	'default' => true],
        ];
    }

	public static function relations(Mapper $mapper, Entity $entity)
    {
        return [
            'carrera' => $mapper->belongsTo($entity, 'Application\Model\Carrera', 'idCarrera'),
            'deporte' => $mapper->belongsTo($entity, 'Application\Model\Deporte', 'idDeporte'),
            'asistencias' => $mapper->hasMany($entity, 'Application\Model\RegistroAsistencia', 'matricula'),
            'certificados' => $mapper->hasMany($entity, 'Application\Model\Certificado', 'matricula'),
        ];
    }

	public function setInputFilter(InputFilterInterface $inputFilter)
     {
         $this->inputFilter = $inputFilter;
     }

     public function getInputFilter()
     {
         if (!$this->inputFilter) {
             $inputFilter = new InputFilter();

             $inputFilter->add(array(
                 'name'     => 'matricula',
                 'required' => true,
                 'filters'  => array(
                     array('name' => 'StripTags'),
                     array('name' => 'StringTrim'),
                 ),
				 'validators' => array(
                     array(
                         'name'    => 'StringLength',
                         'options' => array(
                             'encoding' => 'UTF-8',
                             'min'      => 4,
                             'max'      => 15,
                         ),
                     ),
                     array(
                         'name'    => 'Digits'
                     ),
                 ),
             ));

             $inputFilter->add(array(
                 'name'     => 'nombre',
                 'required' => true,
                 'filters'  => array(
                     array('name' => 'StripTags'),
                     array('name' => 'StringTrim'),
                 ),
                 'validators' => array(
                     array(
                         'name'    => 'StringLength',
                         'options' => array(
                             'encoding' => 'UTF-8',
                             'min'      => 1,
                             'max'      => 50,
                         ),
                     ),
                     array(
                         'name'    => 'Regex',
                         'options' => array(
                             'pattern' => '/[a-z]{1,50}/i',
							 'message' => 'Only characters allowed'
                         ),
                     ),
                 ),
             ));
             $inputFilter->add(array(
                 'name'     => 'apellidos',
                 'required' => true,
                 'filters'  => array(
                     array('name' => 'StripTags'),
                     array('name' => 'StringTrim'),
                 ),
                 'validators' => array(
                     array(
                         'name'    => 'StringLength',
                         'options' => array(
                             'encoding' => 'UTF-8',
                             'min'      => 1,
                             'max'      => 50,
                         ),
                     ),
                     array(
                         'name'    => 'Regex',
                         'options' => array(
                             'pattern' => '/[a-z]{1,50}/i',
							 'message' => 'Only characters allowed'
                         ),
                     ),
                 ),
             ));
			 $inputFilter->add(array(
				 'name'     => 'idCarrera',
				 'required' => true,
				 'filters'  => array(
                     array('name' => 'Digits'),
                 )
			 ));
			 $inputFilter->add(array(
				 'name'     => 'idDeporte',
				 'required' => false,
				 'allow_empty' => true,
				 'filters'  => array(
                     array('name' => 'ToNull'),
                 )
			 ));
			 $inputFilter->add(array(
				 'name'     => 'activo',
				 'required' => true,
				 'filters'  => array(
                     array('name' => 'Boolean'),
                 )
			 ));

             $this->inputFilter = $inputFilter;
         }

         return $this->inputFilter;
    }

	public function getArrayCopy()
	{
		return $this->data();
	}

	public function exchangeArray($array)
	{
		$this->data($array);
	}
}
