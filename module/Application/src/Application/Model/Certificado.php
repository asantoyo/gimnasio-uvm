<?php
namespace Application\Model;

use \Spot\MapperInterface as Mapper;
use \Spot\EntityInterface as Entity;

class Certificado extends \Spot\Entity
{
    protected static $table = 'certificado_medico';

    public static function fields()
    {
        return [
            'idCertificado'	=> ['type' => 'integer',	'primary' => true,	'autoincrement' => true],
            'matricula'	=> ['type' => 'integer',	'required' => true],
            'fecha'	=> ['type' => 'date',	'required' => true],
        ];
    }

	public static function relations(Mapper $mapper, Entity $entity)
    {
        return [
            'alumno' => $mapper->belongsTo($entity, 'Application\Model\Alumno', 'matricula')
        ];
    }
}
