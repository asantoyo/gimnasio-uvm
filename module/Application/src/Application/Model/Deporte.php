<?php
namespace Application\Model;

use \Spot\MapperInterface as Mapper;
use \Spot\EntityInterface as Entity;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Deporte extends \Spot\Entity implements InputFilterAwareInterface
{
    protected static $table = 'deporte';
	protected $inputFilter;

    public static function fields()
    {
        return [
            'idDeporte'	=> ['type' => 'integer',	'primary' => true,	'autoincrement' => true],
            'nombre'	=> ['type' => 'string',	'required' => true]
        ];
    }

	public static function relations(Mapper $mapper, Entity $entity)
    {
        return [
            'alumnos' => $mapper->hasMany($entity, 'Application\Model\Alumno', 'idDeporte'),
        ];
    }

	public function setInputFilter(InputFilterInterface $inputFilter)
	{
		throw new \Exception("Not used");
	}

	public function getInputFilter()
	{
		if (!$this->inputFilter) {
			$inputFilter = new InputFilter();

			$inputFilter->add(array(
				'name'     => 'nombre',
				'required' => true,
				'filters'  => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
					array(
						'name'    => 'StringLength',
						'options' => array(
						'encoding' => 'UTF-8',
						'min'      => 1,
						'max'      => 80,
						),
					),
					array(
						'name'    => 'Regex',
						'options' => array(
							'pattern' => '/[a-z]{1,80}/i',
							'message' => 'Only characters allowed'
						),
					),
				),
			));
			$this->inputFilter = $inputFilter;
		}

		return $this->inputFilter;
	}

	public function getArrayCopy()
	{
		return $this->data();
	}

}
