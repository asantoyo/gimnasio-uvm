<?php
namespace Application\Model;

use \Spot\MapperInterface as Mapper;
use \Spot\EntityInterface as Entity;

use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

class Admin extends \Spot\Entity implements InputFilterAwareInterface
{
    protected static $table = 'admin';
	protected $inputFilter;

    public static function fields()
    {
        return [
            'matricula'	=> ['type' => 'integer',	'primary' => true],
            'nombre'	=> ['type' => 'string',	'required' => true],
            'password'	=> ['type' => 'string',	'required' => true],
            'fechaRegistro'	=> ['type' => 'datetime',	'required' => true],
        ];
    }

	public function setInputFilter(InputFilterInterface $inputFilter)
	{
		throw new \Exception("Not used");
	}

	public function getInputFilter()
	{
		if (!$this->inputFilter) {
			$inputFilter = new InputFilter();

			$inputFilter->add(array(
				'name'     => 'matricula',
				'required' => true,
				'filters'  => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
					array(
						'name'    => 'StringLength',
						'options' => array(
							'encoding' => 'UTF-8',
							'min'      => 4,
							'max'      => 15,
						),
					),
					array(
						'name'    => 'Digits'
					),
				),
			));

			$inputFilter->add(array(
				'name'     => 'nombre',
				'required' => true,
				'filters'  => array(
					array('name' => 'StripTags'),
					array('name' => 'StringTrim'),
				),
				'validators' => array(
					array(
						'name'    => 'StringLength',
						'options' => array(
							'encoding' => 'UTF-8',
							'min'      => 1,
							'max'      => 50,
						),
					),
					array(
						'name'    => 'Regex',
						'options' => array(
							'pattern' => '/[a-z]{1,50}/i',
							'message' => 'Only characters allowed'
						),
					),
				),
			));

			$inputFilter->add(array(
				'name'     => 'password',
				'required' => true,
				'validators' => array(
					array(
						'name'    => 'StringLength',
						'options' => array(
							'encoding' => 'UTF-8',
							'min'      => 1,
							'max'      => 72,
						),
					)
				),
			));

			$inputFilter->add(array(
				'name'     => 'password2',
				'required' => true,
				'validators' => array(
					array(
						'name'    => 'Identical',
						'options' => array(
							'token' => 'password',
						),
					),
				),
			));

			$this->inputFilter = $inputFilter;
		}

		return $this->inputFilter;
	}

	public function getArrayCopy()
	{
		return $this->data();
	}

}
