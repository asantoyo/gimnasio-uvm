<?php
namespace Application\Model;

use \Spot\MapperInterface as Mapper;
use \Spot\EntityInterface as Entity;

class RegistroAsistencia extends \Spot\Entity
{
    protected static $table = 'registro_asistencia';

    public static function fields()
    {
        return [
			'idAsistencia'	=> ['type' => 'integer',	'primary' => true,	'autoincrement' => true],
            'matricula'	=> ['type' => 'integer',	'required' => true,	'primary' => true],
            'horaFecha'	=> ['type' => 'datetime',	'required' => true],
        ];
    }

	public static function relations(Mapper $mapper, Entity $entity)
    {
        return [
            'alumno' => $mapper->belongsTo($entity, 'Application\Model\Alumno', 'matricula')
        ];
    }
}
