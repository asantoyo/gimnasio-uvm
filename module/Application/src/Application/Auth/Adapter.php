<?php
namespace Application\Auth;

use Zend\Authentication\Adapter\AdapterInterface;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Crypt\Password\Bcrypt;
use Zend\Authentication\Result;

class Adapter implements AdapterInterface, ServiceLocatorAwareInterface
{
	private $username;
	private $password;

	private $services;

    /**
     * Sets username and password for authentication
     *
     * @return void
     */
    public function init($username, $password)
    {
        $this->username = $username;
        $this->password = $password;
    }

    /**
     * Performs an authentication attempt
     *
     * @return \Zend\Authentication\Result
     * @throws \Zend\Authentication\Adapter\Exception\ExceptionInterface
     *               If authentication cannot be performed
     */
    public function authenticate()
    {
		if ($this->username == null || $this->password == null) {
			return new Result(Result::FAILURE_IDENTITY_AMBIGUOUS, array(), array('Missing parameters'));
		}
		$sm = $this->getServiceLocator();
		$spot = $this->getServiceLocator()->get('Spot\Locator');
		$empleadoMapper = $spot->mapper('Application\Model\Admin');
		$empleado = $empleadoMapper->first(['matricula' => $this->username]);
		if ($empleado) {
			$bcrypt = new Bcrypt();
			$securePass = $empleado->password;

			if ($bcrypt->verify($this->password, $securePass)) {
				$identity = array(
					'matricula' => $empleado->matricula,
					'nombre' => $empleado->nombre,
					'role' => strtolower('ADMIN'),
				);
				return new Result(Result::SUCCESS, array('identity' => $identity, 'type' => 'admin'));
			} else {
				return new Result(Result::FAILURE_CREDENTIAL_INVALID, array(), array('Wrong password'));
			}
		}
		return new Result(Result::FAILURE_IDENTITY_NOT_FOUND, array(), array('Wrong username'));
    }

	public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->services = $serviceLocator;
    }

    public function getServiceLocator()
    {
        return $this->services;
    }
}
