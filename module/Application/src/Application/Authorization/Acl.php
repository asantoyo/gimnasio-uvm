<?php

namespace Application\Authorization;

use Zend\Permissions\Acl\Acl as ZAcl;
use Zend\Permissions\Acl\Role\GenericRole as Role;
use Zend\Permissions\Acl\Resource\GenericResource as Resource;

class Acl {

	/**
     * Returns the *Singleton* instance of this class.
     *
     * @staticvar Zend\Permissions\Acl\Acl $instance The *Singleton* instances of this class.
     *
     * @return Zend\Permissions\Acl\Acl The *Singleton* instance.
     */
    public static function getACL()
    {
        static $instance = null;
        if (null === $instance) {
			$acl = new ZAcl();

			$guest = new Role('guest');
			$admin = new Role('admin');

			$acl->addRole($guest)
				->addRole($admin);

			$acl->addResource(new Resource('Application\Controller\Registro'));
			$acl->addResource(new Resource('Application\Controller\Checkin'));
			$acl->addResource(new Resource('Application\Controller\Auth'));
			$acl->addResource(new Resource('Application\Controller\Alumnos'));
			$acl->addResource(new Resource('Application\Controller\Metricas'));
			$acl->addResource(new Resource('Application\Controller\Database'));
			$acl->addResource(new Resource('Application\Controller\Admin'));
			$acl->addResource(new Resource('Application\Controller\Import'));

			$acl->deny('guest');
			$acl->allow('guest', 'Application\Controller\Auth');
			$acl->allow('guest', 'Application\Controller\Checkin');
			$acl->allow('admin');

            $instance = $acl;
        }

        return $instance;
    }

	/**
     * Protected constructor to prevent creating a new instance of the
     * *Singleton* via the `new` operator from outside of this class.
     */
    protected function __construct()
    {
    }

    /**
     * Private clone method to prevent cloning of the instance of the
     * *Singleton* instance.
     *
     * @return void
     */
    private function __clone()
    {
    }

    /**
     * Private unserialize method to prevent unserializing of the *Singleton*
     * instance.
     *
     * @return void
     */
    private function __wakeup()
    {
    }
}
