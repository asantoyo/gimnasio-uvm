<?php

namespace Application\Authorization;

use Zend\EventManager\AbstractListenerAggregate;
use Zend\EventManager\EventManagerInterface;
use Zend\Mvc\MvcEvent;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Session\Container;

use Application\Authorization\Acl;

class AuthListener extends AbstractListenerAggregate implements ServiceLocatorAwareInterface {

	const ERROR = 'error-unauthorized';

	protected $serviceLocator;

	public function attach(EventManagerInterface $events)
    {
        $this->listeners[] = $events->attach(MvcEvent::EVENT_ROUTE, array($this, 'onDispatch'), -1000);
    }

	public function onDispatch(MvcEvent $event)
    {
		$storage = new Container('UVM');
		if ($storage->lastLogin != null) {
			$diff = (int) ($storage->lastLogin->diff(new \DateTime())->format('%h'));
		} else {
			$diff = 0;
		}
		$auth 		= $this->serviceLocator->get('Zend\Authentication\AuthenticationService');
		if ($diff < 8) {
			$acl 		= Acl::getACL();
			$identity 	= ($auth->hasIdentity()) ? $auth->getIdentity()['identity'] : array('role' => 'guest');

			$match      = $event->getRouteMatch();
			$controller = $match->getParam('controller');
			$action     = $match->getParam('action');
			$request    = $event->getRequest();
			$method     = $request instanceof HttpRequest ? strtolower($request->getMethod()) : null;

			$authorized = $acl->isAllowed($identity['role'], $controller);
				//|| $acl->isAllowed($this->getResourceName($controller, $action))
				//|| ($method && $acl->isAllowed($this->getResourceName($controller, $method)));
			if ($authorized) {
				return;
			}
		} else {
			$auth->clearIdentity();
			$storage->lastLogin = null;
		}

		//  Assuming your login route has a name 'login', this will do the assembly
		// (you can also use directly $url=/path/to/login)
		$url = $event->getRouter()->assemble(array(), array('name' => 'login'));
		$response=$event->getResponse();
		$response->getHeaders()->addHeaderLine('Location', $url);
		$response->setStatusCode(302);
		$response->sendHeaders();
		// When an MvcEvent Listener returns a Response object,
		// It automatically short-circuit the Application running
		// -> true only for Route Event propagation see Zend\Mvc\Application::run

		// To avoid additional processing
		// we can attach a listener for Event Route with a high priority
		$stopCallBack = function($event) use ($response){
			$event->stopPropagation();
			return $response;
		};
		//Attach the "break" as a listener with a high priority
		$event->getApplication()->getEventManager()->attach(MvcEvent::EVENT_ROUTE, $stopCallBack,-10000);
		return $response;
/*
        $event->setError(static::ERROR);
        $event->setParam('identity', $identity);
        $event->setParam('controller', $controller);
        $event->setParam('action', $action);
        $errorMessage = sprintf("You are not authorized to access %s:%s", $controller, $action);
        $event->setParam('exception', new \BadMethodCallException($errorMessage));

		$app = $event->getTarget();
        $app->getEventManager()->trigger(MvcEvent::EVENT_DISPATCH_ERROR, $event);
*/
    }

	public function setServiceLocator(ServiceLocatorInterface $serviceLocator)
    {
        $this->serviceLocator = $serviceLocator;
    }

    public function getServiceLocator()
    {
        return $this->serviceLocator;
    }
}
