<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\JsonModel;

class DatabaseController extends AbstractActionController
{
    public function indexAction()
    {
		$sl = $this->getServiceLocator();
        $carrera = $sl->get('FormElementManager')->get('\Application\Form\CarreraForm');
        $deporte = $sl->get('FormElementManager')->get('\Application\Form\DeporteForm');
		return array(
			'carrera' => $carrera,
			'deporte' => $deporte
		);
    }

	public function allCarrerasAction () {
		$spot = $this->getServiceLocator()->get('Spot\Locator');
		$carreraMapper = $spot->mapper('Application\Model\Carrera');

		$data = array();
		foreach($carreraMapper->all() as $carrera) {
			$carreraData = array();
			$carreraData[] = $carrera->nombre;
			$carreraData[] = $carrera->idCarrera;
			$data[] = $carreraData;
		}

		$result = new JsonModel(array(
	    	'data' => $data
        ));

        return $result;
	}

	public function editcarreraAction() {
		$sl = $this->getServiceLocator();
        $form = $sl->get('FormElementManager')->get('\Application\Form\CarreraForm');

		$request = $this->getRequest();
        $carrera = $request->getQuery('carrera');

		$spot = $this->getServiceLocator()->get('Spot\Locator');
		$carreraMapper = $spot->mapper('Application\Model\Carrera');
		$carrera = $carreraMapper->get($carrera);
		if ($carrera) {
			$form->setInputFilter($carrera->getInputFilter());
			$form->setData($carrera->data());
		}

        return array(
        	'form' => $form,
        	'carrera' => $carrera,
			'error' => ($carrera->hasErrors("nombre")) ? $carrera->errors("nombre") : null
        );
	}

	public function editcarrerasaveAction() {
		$sl = $this->getServiceLocator();
        $form = $sl->get('FormElementManager')->get('\Application\Form\CarreraForm');

        $request = $this->getRequest();
		$carrera = $request->getQuery('carrera');
        if ($request->isPost()) {
            $spot = $this->getServiceLocator()->get('Spot\Locator');
			$carreraMapper = $spot->mapper('Application\Model\Carrera');
			$carrera = $carreraMapper->get($carrera);
            $form->setInputFilter($carrera->getInputFilter());
            $form->setData($request->getPost());
			$form->isValid();
            if ($form->isValid()) {
                $carrera->data($form->getData());
				$palabras = explode(' ', $carrera->nombre);
				foreach($palabras as $k=>$palabra) {
					$palabras[$k] = ucfirst(strtolower($palabra));
				}
				$carrera->nombre = implode(' ', $palabras);
				$connection = $spot->config()->connection();
				$connection->beginTransaction();
				try {
					// Save
					$result = $carreraMapper->update($carrera);
					if ($result) {
						$connection->commit();
						return $this->redirect()->toRoute('application/default', array('controller' => 'database', 'action' => 'index'));
					} else {
						$form->bind($carrera);
					}
					$connection->rollback();
				} catch(Exception $e) {
					$connection->rollback();
					throw $e;
				}
            }
        }
        return array(
        	'form' => $form,
			'carrera' => $carrera,
			'error' => ($carrera->hasErrors("nombre")) ? $carrera->errors("nombre") : null
        );
	}

	public function newcarreraAction()
	{
		$sl = $this->getServiceLocator();
        $form = $sl->get('FormElementManager')->get('\Application\Form\CarreraForm');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $spot = $this->getServiceLocator()->get('Spot\Locator');
			$carreraMapper = $spot->mapper('Application\Model\Carrera');
			$carrera = $carreraMapper->build([]);
            $form->setInputFilter($carrera->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $carrera->data($form->getData());
				$palabras = explode(' ', $carrera->nombre);
				foreach($palabras as $k=>$palabra) {
					$palabras[$k] = ucfirst(strtolower($palabra));
				}
				$carrera->nombre = implode(' ', $palabras);
				$connection = $spot->config()->connection();
				$connection->beginTransaction();
				try {
					// Save
					$result = $carreraMapper->save($carrera);
					if ($result) {
						$connection->commit();
						return $this->redirect()->toRoute('application/default', array('controller' => 'database', 'action' => 'index'));
					} else {
						$form->bind($carrera);
					}
					$connection->rollback();
				} catch(Exception $e) {
					$connection->rollback();
					throw $e;
				}
            }
        }
        return array(
        	'form' => $form,
			'error' => ($carrera->hasErrors("nombre")) ? $carrera->errors("nombre") : null
        );
	}

	public function allDeportesAction () {
		$spot = $this->getServiceLocator()->get('Spot\Locator');
		$deporteMapper = $spot->mapper('Application\Model\Deporte');

		$data = array();
		foreach($deporteMapper->all() as $deporte) {
			$deporteData = array();
			$deporteData[] = $deporte->nombre;
			$deporteData[] = $deporte->idDeporte;
			$data[] = $deporteData;
		}

		$result = new JsonModel(array(
	    	'data' => $data
        ));

        return $result;
	}

	public function editdeporteAction() {
		$sl = $this->getServiceLocator();
        $form = $sl->get('FormElementManager')->get('\Application\Form\DeporteForm');

		$request = $this->getRequest();
        $deporte = $request->getQuery('deporte');

		$spot = $this->getServiceLocator()->get('Spot\Locator');
		$deporteMapper = $spot->mapper('Application\Model\Deporte');
		$deporte = $deporteMapper->get($deporte);
		if ($deporte) {
			$form->setInputFilter($deporte->getInputFilter());
			$form->setData($deporte->data());
		}

        return array(
        	'form' => $form,
        	'deporte' => $deporte,
			'error' => ($deporte->hasErrors("nombre")) ? $deporte->errors("nombre") : null
        );
	}

	public function editdeportesaveAction() {
		$sl = $this->getServiceLocator();
        $form = $sl->get('FormElementManager')->get('\Application\Form\DeporteForm');

        $request = $this->getRequest();
		$deporte = $request->getQuery('deporte');
        if ($request->isPost()) {
            $spot = $this->getServiceLocator()->get('Spot\Locator');
			$deporteMapper = $spot->mapper('Application\Model\Deporte');
			$deporte = $deporteMapper->get($deporte);
            $form->setInputFilter($deporte->getInputFilter());
            $form->setData($request->getPost());
			$form->isValid();
            if ($form->isValid()) {
                $deporte->data($form->getData());
				$palabras = explode(' ', $deporte->nombre);
				foreach($palabras as $k=>$palabra) {
					$palabras[$k] = ucfirst(strtolower($palabra));
				}
				$deporte->nombre = implode(' ', $palabras);
				$connection = $spot->config()->connection();
				$connection->beginTransaction();
				try {
					// Save
					$result = $deporteMapper->update($deporte);
					if ($result) {
						$connection->commit();
						return $this->redirect()->toRoute('application/default', array('controller' => 'database', 'action' => 'index'));
					} else {
						$form->bind($deporte);
					}
					$connection->rollback();
				} catch(Exception $e) {
					$connection->rollback();
					throw $e;
				}
            }
        }
        return array(
        	'form' => $form,
			'deporte' => $deporte,
			'error' => ($deporte->hasErrors("nombre")) ? $deporte->errors("nombre") : null
        );
	}

	public function newdeporteAction()
	{
		$sl = $this->getServiceLocator();
        $form = $sl->get('FormElementManager')->get('\Application\Form\DeporteForm');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $spot = $this->getServiceLocator()->get('Spot\Locator');
			$deporteMapper = $spot->mapper('Application\Model\Deporte');
			$deporte = $deporteMapper->build([]);
            $form->setInputFilter($deporte->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $deporte->data($form->getData());
				$palabras = explode(' ', $deporte->nombre);
				foreach($palabras as $k=>$palabra) {
					$palabras[$k] = ucfirst(strtolower($palabra));
				}
				$deporte->nombre = implode(' ', $palabras);
				$connection = $spot->config()->connection();
				$connection->beginTransaction();
				try {
					// Save
					$result = $deporteMapper->save($deporte);
					if ($result) {
						$connection->commit();
						return $this->redirect()->toRoute('application/default', array('controller' => 'database', 'action' => 'index'));
					} else {
						$form->bind($deporte);
					}
					$connection->rollback();
				} catch(Exception $e) {
					$connection->rollback();
					throw $e;
				}
            }
        }
        return array(
        	'form' => $form,
			'error' => ($deporte->hasErrors("nombre")) ? $deporte->errors("nombre") : null
        );
	}

}
