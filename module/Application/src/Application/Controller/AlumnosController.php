<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\View\Model\JsonModel;

class AlumnosController extends AbstractActionController
{
    public function indexAction()
    {
		return new ViewModel();
    }

	public function allAlumnosAction () {
		setlocale(LC_TIME, 'es_ES');
		$spot = $this->getServiceLocator()->get('Spot\Locator');
		$alumnoMapper = $spot->mapper('Application\Model\Alumno');
		if(intval($alumnoMapper->query('SELECT COUNT(*) AS count FROM alumno')[0]->count) > 3600) {
			$alumnos = $alumnoMapper->select()->order(['matricula' => 'DESC'])->limit(3600);
		} else {
			$alumnos = $alumnoMapper->all();
		}
		$data = array();
		foreach($alumnos as $alumno) {
			$alumnoData = array();
			$alumnoData[] = $alumno->nombre . ' ' . $alumno->apellidos;
			$alumnoData[] = $alumno->matricula;
			$alumnoData[] = $alumno->carrera->nombre;
			$alumnoData[] = $alumno->deporte->nombre;
			$certificado = $alumno->certificados->order(['fecha' => 'DESC'])->execute()->first();
			$fecha = ($certificado) ? $certificado->fecha : new \Datetime();
			$alumnoData[] = strftime('%d %B %Y', $fecha->getTimestamp());
			$diff = $fecha->diff(new \DateTime());
			$alumnoData[] = $diff->format('%y');
			$alumnoData[] = $alumno->activo;
			$data[] = $alumnoData;
		}

		$result = new JsonModel(array(
	    	'data' => $data
        ));

        return $result;
	}

	public function toggleActivateAction () {
		setlocale(LC_TIME, 'es_ES');
		$spot = $this->getServiceLocator()->get('Spot\Locator');
		$alumnoMapper = $spot->mapper('Application\Model\Alumno');

		$data = array('status' => 'error');
		$request = $this->getRequest();
        if ($request->isPost()) {
			$matricula = $request->getPost('matricula');
			$alumno = $alumnoMapper->get($matricula);
			if ($alumno) {
				$connection = $spot->config()->connection();
				$connection->beginTransaction();
				try {
					$alumno->activo = !$alumno->activo;
					$alumnoMapper->update($alumno);
					if ($alumno->activo) {
						$certificadoMapper = $spot->mapper('Application\Model\Certificado');
						$date = new \DateTime();
						$certificado = $certificadoMapper->build(['matricula' => $alumno->matricula, 'fecha' => $date]);
						$result = $certificadoMapper->save($certificado);
						if ($result) {
							$connection->commit();
							$fecha = $certificado->fecha;
							$diff = $fecha->diff(new \DateTime());
							$data = array('status' => 'ok', 'active' => $alumno->activo, 'fecha' => strftime('%d %B %Y', $certificado->fecha->getTimestamp()), 'diff' => $diff->format('%y'));
						} else {
							$connection->rollback();
						}
					} else {
						$connection->commit();
						$data = array('status' => 'ok', 'active' => $alumno->activo);
					}
				} catch(Exception $e) {
					$connection->rollback();
					throw $e;
				}
			}
		}

		$result = new JsonModel($data);

        return $result;
	}

	public function renewAction() {
		setlocale(LC_TIME, 'es_ES');
		$spot = $this->getServiceLocator()->get('Spot\Locator');
		$alumnoMapper = $spot->mapper('Application\Model\Alumno');

		$data = array('status' => 'error');
		$request = $this->getRequest();
        if ($request->isPost()) {
			$matricula = $request->getPost('matricula');
			$alumno = $alumnoMapper->get($matricula);
			if ($alumno) {
				$certificado = $alumno->certificados->order(['fecha' => 'DESC'])->execute()->first();
				$fecha = $certificado->fecha;
				$diff = $fecha->diff(new \DateTime());
				if (intval($diff->format('%y')) >= 1) {
					$certificadoMapper = $spot->mapper('Application\Model\Certificado');
					$date = new \DateTime();
					$connection = $spot->config()->connection();
					$connection->beginTransaction();
					try {
						$certificado = $certificadoMapper->build(['matricula' => $alumno->matricula, 'fecha' => $date]);
						$result = $certificadoMapper->save($certificado);
						if ($result) {
							$connection->commit();
							$fecha = $certificado->fecha;
							$diff = $fecha->diff(new \DateTime());
							$data = array('status' => 'ok', 'fecha' => strftime('%d %B %Y', $certificado->fecha->getTimestamp()), 'diff' => $diff->format('%y'));
						} else {
							$connection->rollback();
						}
					} catch(Exception $e) {
						$connection->rollback();
						throw $e;
					}
				}
			}
		}

		$result = new JsonModel($data);

        return $result;
	}

	public function editAction() {
		$sl = $this->getServiceLocator();
        $form = $sl->get('FormElementManager')->get('\Application\Form\AlumnoForm');

		$request = $this->getRequest();
        $matricula = $request->getQuery('matricula');

		$spot = $this->getServiceLocator()->get('Spot\Locator');
		$alumnoMapper = $spot->mapper('Application\Model\Alumno');
		$alumno = $alumnoMapper->get($matricula);
		if ($alumno) {
			$form->setInputFilter($alumno->getInputFilter());
			$form->setData($alumno->data());
			$certificado = $alumno->certificados->order(['fecha' => 'DESC'])->execute()->first();
			$fecha = ($certificado) ? $certificado->fecha : new \Datetime();
		}

        return array(
        	'form' => $form,
			'fecha' => $fecha,
			'error' => ($alumno->hasErrors("matricula")) ? $alumno->errors("matricula") : null
        );
	}

	public function editsaveAction() {
		$sl = $this->getServiceLocator();
        $form = $sl->get('FormElementManager')->get('\Application\Form\AlumnoForm');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $spot = $this->getServiceLocator()->get('Spot\Locator');
			$alumnoMapper = $spot->mapper('Application\Model\Alumno');
			$alumno = $alumnoMapper->get($request->getPost('matricula'));
            $form->setInputFilter($alumno->getInputFilter());
            $form->setData($request->getPost());
			$form->isValid();
            if ($form->isValid()) {
                $alumno->data($form->getData());
				$palabras = explode(' ', $alumno->nombre);
				foreach($palabras as $k=>$palabra) {
					$palabras[$k] = ucfirst(strtolower($palabra));
				}
				$alumno->nombre = implode(' ', $palabras);
				$palabras = explode(' ', $alumno->apellidos);
				foreach($palabras as $k=>$palabra) {
					$palabras[$k] = ucfirst(strtolower($palabra));
				}
				$alumno->apellidos = implode(' ', $palabras);
				$certificadoMapper = $spot->mapper('Application\Model\Certificado');
				$date = \DateTime::createFromFormat('d/m/Y', $request->getPost('certificado'));
				$connection = $spot->config()->connection();
				$connection->beginTransaction();
				try {
					// Save
					$result = $certificadoMapper->delete(['fecha >' => $date->format('Y-m-d'), 'matricula' => $alumno->matricula]);
					$certificado = $certificadoMapper->build(['matricula' => $alumno->matricula, 'fecha' => $date]);
					$result = $certificadoMapper->save($certificado);
					$result2 = $alumnoMapper->update($alumno);
					if ($result || $result2) {
						$connection->commit();
						return $this->redirect()->toRoute('application/default', array('controller' => 'alumnos', 'action' => 'index'));
					} else {
						$form->bind($alumno);
					}
					$connection->rollback();
				} catch(Exception $e) {
					$connection->rollback();
					throw $e;
				}
            }
        }
        return array(
        	'form' => $form,
			'error' => ($alumno->hasErrors("matricula")) ? $alumno->errors("matricula") : null
        );
	}

}
