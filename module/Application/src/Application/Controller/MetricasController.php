<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;

class MetricasController extends AbstractActionController
{
    public function indexAction()
    {
		$spot = $this->getServiceLocator()->get('Spot\Locator');
		$carreraMapper = $spot->mapper('Application\Model\Carrera');
		$series = array();
		foreach($carreraMapper->all() as $carrera) {
			$alumnos = $carrera->alumnos;
			$count = $alumnos->where(['activo' => 1])->count();
			$serie = array('name' => $carrera->nombre, 'data' => array($count));
			if($count == 0) {
				$serie['visible'] = false;
			}
			$series[] = $serie;
		}

		$asistenciasMapper = $spot->mapper('Application\Model\RegistroAsistencia');
		$asistencias = $asistenciasMapper->query('SELECT HOUR(`horaFecha`) as hour, WEEKDAY(`horaFecha`) as day, COUNT(*) as count FROM `registro_asistencia` GROUP BY WEEKDAY(`horaFecha`), HOUR(`horaFecha`)');

		$hours = array();
		for($j = 0; $j < 24; $j++) {
			$hours[] = strval($j);
		}

		setlocale(LC_TIME, 'es_ES');
		$days = array();
		for($i = 0; $i < 7; $i++) {
			$days[] = ucfirst(strftime('%A', strtotime("Monday +{$i} days")));
		}

		$data = array();
		$max = 0;
		foreach($asistencias as $a) {
			$data[] = array((int)$a->hour, (int)$a->day, (int)$a->count);
			if ((int)$a->count > $max) {
				$max = (int)$a->count;
			}
		}

		return array(
			'series' => $series,
			'hours' => $hours,
			'days' => $days,
			'data' => $data,
			'max' => $max
		);
    }

}
