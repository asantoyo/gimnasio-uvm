<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Crypt\Password\Bcrypt;

class AdminController extends AbstractActionController
{
    public function indexAction()
    {
		$sl = $this->getServiceLocator();
        $form = $sl->get('FormElementManager')->get('\Application\Form\AdminForm');
        return array(
			'form' => $form
		);
    }

	public function newadminAction() {
		$sl = $this->getServiceLocator();
        $form = $sl->get('FormElementManager')->get('\Application\Form\AdminForm');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $spot = $this->getServiceLocator()->get('Spot\Locator');
			$adminMapper = $spot->mapper('Application\Model\Admin');
			$admin = $adminMapper->build([]);
            $form->setInputFilter($admin->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
				$data = $form->getData();
				$admin->matricula = $data['matricula'];
				$admin->nombre = $data['nombre'];
				$palabras = explode(' ', $admin->nombre);
				foreach($palabras as $k=>$palabra) {
					$palabras[$k] = ucfirst(strtolower($palabra));
				}
				$admin->nombre = implode(' ', $palabras);
				$bcrypt = new Bcrypt();
				$securePass = $bcrypt->create($data['password']);
				$admin->password = $securePass;
				$admin->fechaRegistro = new \Datetime();
				$connection = $spot->config()->connection();
				$connection->beginTransaction();
				try {
					// Save
					$result = $adminMapper->save($admin);
					if ($result) {
						$connection->commit();
						return $this->redirect()->toRoute('home');
					} else {
						$form->bind($admin);
					}
					$connection->rollback();
				} catch(Exception $e) {
					$connection->rollback();
					throw $e;
				}
            }
        }
        return array(
        	'form' => $form,
			'error' => ($admin->hasErrors("password")) ? $admin->errors("password") : null
        );
	}
}
