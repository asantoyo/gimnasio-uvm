<?php

namespace Application\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\Permissions\Acl\Acl;

class IsAllowed extends AbstractPlugin
{

    protected $authorizeService;

	public function __construct(Acl $authorizeService)
    {
        $this->authorizeService = $authorizeService;
    }

	public function __invoke($identity, $resource = null, $action = null)
    {
        return $this->authorizeService->isAllowed($identity, $resource, $action);
    }
}
