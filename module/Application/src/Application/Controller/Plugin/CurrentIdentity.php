<?php

namespace Application\Controller\Plugin;

use Zend\Mvc\Controller\Plugin\AbstractPlugin;
use Zend\Authentication\AuthenticationService;

class CurrentIdentity extends AbstractPlugin
{

	protected $authorizeService;

	public function __construct(AuthenticationService $authorizeService)
    {
        $this->authorizeService = $authorizeService;
    }

	public function __invoke()
    {
		$identity = ($this->authorizeService->hasIdentity()) ? $this->authorizeService->getIdentity()['identity']['role'] : 'guest';
        return $identity;
    }

}
