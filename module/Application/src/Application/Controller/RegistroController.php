<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;

class RegistroController extends AbstractActionController
{
    public function indexAction()
    {
		$sl = $this->getServiceLocator();
        $form = $sl->get('FormElementManager')->get('\Application\Form\AlumnoForm');
        return array(
        	'form' => $form,
        );
    }

	public function registrarAction()
	{
		$sl = $this->getServiceLocator();
        $form = $sl->get('FormElementManager')->get('\Application\Form\AlumnoForm');

        $request = $this->getRequest();
        if ($request->isPost()) {
            $spot = $this->getServiceLocator()->get('Spot\Locator');
			$alumnoMapper = $spot->mapper('Application\Model\Alumno');
			$alumno = $alumnoMapper->build([]);
            $form->setInputFilter($alumno->getInputFilter());
            $form->setData($request->getPost());

            if ($form->isValid()) {
                $alumno->data($form->getData());
				$palabras = explode(' ', $alumno->nombre);
				foreach($palabras as $k=>$palabra) {
					$palabras[$k] = ucfirst(strtolower($palabra));
				}
				$alumno->nombre = implode(' ', $palabras);
				$palabras = explode(' ', $alumno->apellidos);
				foreach($palabras as $k=>$palabra) {
					$palabras[$k] = ucfirst(strtolower($palabra));
				}
				$alumno->apellidos = implode(' ', $palabras);
				$connection = $spot->config()->connection();
				$connection->beginTransaction();
				try {
					// Save
					$result = $alumnoMapper->save($alumno);
					if ($result) {
						$certificadoMapper = $spot->mapper('Application\Model\Certificado');
						$date = \DateTime::createFromFormat('d/m/Y', $request->getPost('certificado'));
						$certificado = $certificadoMapper->build(['matricula' => $alumno->matricula, 'fecha' => $date]);
						$result = $certificadoMapper->save($certificado);
						if ($result) {
							$connection->commit();
							return $this->redirect()->toRoute('application/default', array('controller' => 'registro', 'action' => 'correcto'), array('query' => array('matricula' => $alumno->matricula)));
						}
					} else {
						$form->bind($alumno);
					}
					$connection->rollback();
				} catch(Exception $e) {
					$connection->rollback();
					throw $e;
				}
            }
        }
        return array(
        	'form' => $form,
			'error' => ($alumno->hasErrors("matricula")) ? $alumno->errors("matricula") : null
        );
	}

	public function correctoAction()
	{
        $request = $this->getRequest();
        $matricula = $request->getQuery('matricula');

		$spot = $this->getServiceLocator()->get('Spot\Locator');
		$alumnoMapper = $spot->mapper('Application\Model\Alumno');
		$alumno = $alumnoMapper->get($matricula);
		return array(
        	'alumno' => $alumno,
        );
	}
}
