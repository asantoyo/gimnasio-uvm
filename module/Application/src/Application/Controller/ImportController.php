<?php
namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

use PHPExcel\PHPExcel_IOFactory;
use PHPExcel\Shared\Date;

\PHPExcel_Shared_Date::ExcelToPHPObject();

class ImportController extends AbstractActionController
{
    public function indexAction()
    {
		return new ViewModel();
    }

	public function importAction()
	{
		set_time_limit(0);
		$uploadOk = 1;
		$fileType = pathinfo($_FILES["archivo"]["name"],PATHINFO_EXTENSION);
		$request = $this->getRequest();
        if ($request->isPost()) {
			if($fileType != "xls" && $fileType != "xlsx") {
				$uploadOk = 0;
			}
			if ($uploadOk == 1) {
				$spot = $this->getServiceLocator()->get('Spot\Locator');
				$connection = $spot->config()->connection();

				$alumnoMapper = $spot->mapper('Application\Model\Alumno');
				$carreraMapper = $spot->mapper('Application\Model\Carrera');
				$deporteMapper = $spot->mapper('Application\Model\Deporte');
				$certificadoMapper = $spot->mapper('Application\Model\Certificado');

				$objPHPExcel = \PHPExcel_IOFactory::load($_FILES["archivo"]["tmp_name"]);
				$sheetData = $objPHPExcel->getActiveSheet()->toArray(null,true,true,false);
				foreach($sheetData as $row) {
					$connection->beginTransaction();
					try {
						$apellido = array();
						$nombre = array();
						$n = explode(' ', preg_replace('/\s+/i', ' ', $row[0]));
						for($i = 0; $i < ceil(count($n) / 2); $i++) {
							$apellido[] = ucfirst(strtolower($n[$i]));
						}
						$apellido = implode(' ', $apellido);
						for(; $i < count($n); $i++) {
							$nombre[] = ucfirst(strtolower($n[$i]));
						}
						$nombre = implode(' ', $nombre);

						$matricula		= $row[1];
						$carrera		= $row[2];
						$deporte		= $row[3];
						$certificado	= $row[4];

						$palabras = explode(' ', $carrera);
						foreach($palabras as $k=>$palabra) {
							if(strpos($palabra, '.') === false) {
								$palabras[$k] = ucfirst(strtolower($palabra));
							}
						}
						$carrera = implode(' ', $palabras);
						$carreraDB = $carreraMapper->query("SELECT idCarrera, nombre FROM carrera WHERE LOWER(nombre) LIKE LOWER('%".$carrera."%') ORDER BY nombre ASC")->first();
						if ($carreraDB == null) {
							$palabras = explode(' ', $carrera);
							foreach($palabras as $k=>$palabra) {
								$palabras[$k] = ucfirst(strtolower($palabra));
							}
							$carrera = implode(' ', $palabras);
							$carreraDB = $carreraMapper->create(['nombre' => $carrera]);
						}

						if ($deporte != null && $deporte != '') {
							$deporteDB = $deporteMapper->query("SELECT idDeporte, nombre FROM deporte WHERE LOWER(nombre) LIKE LOWER('%".$deporte."%') ORDER BY nombre ASC")->first();
							if ($deporteDB == null) {
								$palabras = explode(' ', $deporte);
								foreach($palabras as $k=>$palabra) {
									$palabras[$k] = ucfirst(strtolower($palabra));
								}
								$deporte = implode(' ', $palabras);
								$deporteDB = $deporteMapper->create(['nombre' => $deporte]);
							}
						}

						$palabras = explode(' ', $nombre);
						foreach($palabras as $k=>$palabra) {
							$palabras[$k] = ucfirst(strtolower($palabra));
						}
						$nombre = implode(' ', $palabras);
						$palabras = explode(' ', $apellido);
						foreach($palabras as $k=>$palabra) {
							$palabras[$k] = ucfirst(strtolower($palabra));
						}
						$apellido = implode(' ', $palabras);

						$alumnoDB = $alumnoMapper->insert([
							'matricula' => intval($matricula),
							'nombre' => $nombre,
							'apellidos' => $apellido,
							'idCarrera' => intval($carreraDB->idCarrera),
							'idDeporte' => (isset($deporteDB)) ? intval($deporteDB->idDeporte) : null,
							'activo' => true
						]);

						if($alumnoDB !== false) {
							$date = \DateTime::createFromFormat('m/d/Y', $certificado);
							$certificadoDB = $certificadoMapper->create(['matricula' => intval($matricula), 'fecha' => ($date != null) ? $date : new \DateTime()]);
						}

						$connection->commit();
					} catch(Exception $e) {
						$connection->rollback();
						var_dump($row);
						echo $e->getMessage();
					}
				}
			}
		}
		return $this->redirect()->toRoute('application/default', array('controller' => 'import', 'action' => 'index'));
	}
}
