<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;
use Zend\Session\Container;

class AuthController extends AbstractActionController
{
    public function indexAction()
    {
		$sl = $this->getServiceLocator();
        $form = $sl->get('FormElementManager')->get('\Application\Form\LoginForm');
		return array(
			'form' => $form
		);
    }

	public function loginAction() {
		$sl = $this->getServiceLocator();
        $form = $sl->get('FormElementManager')->get('\Application\Form\LoginForm');
		$auth = $this->getServiceLocator()->get('Zend\Authentication\AuthenticationService');
		if ($auth->hasIdentity()) {
			return $this->redirect()->toRoute('home');
		} else {
			$request = $this->getRequest();
			if ($request->isPost()) {
				$form->setData($request->getPost());
				if ($form->isValid()) {
					$authAdapter = $this->getServiceLocator()->get('Application\Auth\Adapter');
					$data = $form->getData();
					$authAdapter->init($data['username'], $data['password']);
					$result = $auth->authenticate($authAdapter);
					if (!$result->isValid()) {
						// Authentication failed; print the reasons why
						foreach ($result->getMessages() as $message) {
							$error[] = "$message";
						}
					} else {
						$storage = new Container('UVM');
						$storage->lastLogin = new \Datetime();
						return $this->redirect()->toRoute('home');
					}
				}
			}
		}
		return array(
			'form' => $form,
			'error' => (isset($error) ? $error : null)
		);
	}

	public function logoutAction () {
		$auth = $this->getServiceLocator()->get('Zend\Authentication\AuthenticationService');
		$auth->clearIdentity();
		return $this->redirect()->toRoute('application/default', array('controller' => 'auth'));
	}
}
