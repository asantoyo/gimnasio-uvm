<?php

namespace Application\Controller;

use Zend\Mvc\Controller\AbstractActionController;
use Zend\View\Model\ViewModel;

class CheckinController extends AbstractActionController
{
    public function indexAction()
    {
        $sl = $this->getServiceLocator();
        $form = $sl->get('FormElementManager')->get('\Application\Form\AsistenciaForm');
		return array(
			'form' => $form
		);
    }

	public function registroAction() {
		$sl = $this->getServiceLocator();
        $form = $sl->get('FormElementManager')->get('\Application\Form\AsistenciaForm');
		$request = $this->getRequest();
		if ($request->isPost()) {
			$form->setData($request->getPost());
			if ($form->isValid()) {
				$data = $form->getData();

				$spot = $this->getServiceLocator()->get('Spot\Locator');
				$asistenciaMapper = $spot->mapper('Application\Model\RegistroAsistencia');
				$alumnoMapper = $spot->mapper('Application\Model\Alumno');

				$alumno = $alumnoMapper->get($data['matricula']);
				if ($alumno) {
					$fecha = $alumno->certificados->order(['fecha' => 'DESC'])->execute()->first()->fecha;
					$diff = (int) $fecha->diff(new \DateTime())->format('%y');
				}
				if ($alumno && $alumno->activo && $diff < 1) {
					$asistencia = $asistenciaMapper->build(['matricula' => $alumno->matricula, 'horaFecha' => new \DateTime()]);
					// Save
					$result = $asistenciaMapper->save($asistencia);
					if ($result) {
						return $this->redirect()->toRoute('application/default', array('controller' => 'checkin', 'action' => 'correcto'), array('query' => array('matricula' => $alumno->matricula)));
					}
				} else {
					$error = "Student not registered";
				}
			}
		}
		return array(
			'form' => $form,
			'error' => (isset($error) ? $error : null)
		);
	}

	public function correctoAction()
	{
        $request = $this->getRequest();
        $matricula = $request->getQuery('matricula');

		$spot = $this->getServiceLocator()->get('Spot\Locator');
		$alumnoMapper = $spot->mapper('Application\Model\Alumno');
		$alumno = $alumnoMapper->get($matricula);

		$asistencia = $alumno->asistencias->order(['horaFecha' => 'DESC'])->execute()->first();
		return array(
        	'alumno' => $alumno,
        	'asistencia' => $asistencia,
        );
	}
}
