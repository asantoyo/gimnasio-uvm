<?php

namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\Permissions\Acl\Acl;

class IsAllowed extends AbstractHelper
{

    protected $authorizeService;

	public function __construct(Acl $authorizeService)
    {
        $this->authorizeService = $authorizeService;
    }

	public function __invoke($identity, $resource = null, $action = null)
    {
        return $this->authorizeService->isAllowed($identity, $resource, $action);
    }
}
