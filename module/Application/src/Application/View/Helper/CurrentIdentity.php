<?php

namespace Application\View\Helper;

use Zend\View\Helper\AbstractHelper;
use Zend\Authentication\AuthenticationService;

class CurrentIdentity extends AbstractHelper
{

	protected $authorizeService;

	public function __construct(AuthenticationService $authorizeService)
    {
        $this->authorizeService = $authorizeService;
    }

	public function __invoke()
    {
		$identity = ($this->authorizeService->hasIdentity()) ? $this->authorizeService->getIdentity()['identity'] : array('role' => 'guest');
        return $identity;
    }

}
