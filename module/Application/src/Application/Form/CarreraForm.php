<?php
namespace Application\Form;

use Zend\Form\Form;

class CarreraForm extends Form
{
    public function init()
    {
        $this->add(array(
            'name' => 'nombre',
            'type' => 'Text',
            'options' => array(
                'label' => 'Carrera',
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Agregar',
                'id' => 'submitbutton',
            ),
        ));

		$this->setValidationGroup(array(
			'nombre'
		));
    }

}
