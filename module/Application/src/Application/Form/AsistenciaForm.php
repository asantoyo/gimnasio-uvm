<?php
namespace Application\Form;

use Zend\Form\Form;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\InputFilterAwareInterface;
use Zend\InputFilter\InputFilterInterface;

 class AsistenciaForm extends Form implements InputFilterAwareInterface
 {
	 protected $inputFilter;

     public function init()
     {
         $this->add(array(
             'name' => 'matricula',
             'type' => 'Text',
             'options' => array(
                 'label' => 'Matricula',
             ),
         ));
         $this->add(array(
             'name' => 'submit',
             'type' => 'Submit',
             'attributes' => array(
                 'value' => 'Registro',
             ),
         ));
     }

     public function setInputFilter(InputFilterInterface $inputFilter)
	 {
		 $this->inputFilter = $inputFilter;
	 }

	 public function getInputFilter()
     {
         if (!$this->inputFilter) {
             $inputFilter = new InputFilter();

             $inputFilter->add(array(
                 'name'     => 'matricula',
                 'required' => true,
                 'filters'  => array(
                     array('name' => 'StripTags'),
                     array('name' => 'StringTrim'),
                 ),
				 'validators' => array(
                     array(
                         'name'    => 'StringLength',
                         'options' => array(
                             'encoding' => 'UTF-8',
                             'min'      => 4,
                             'max'      => 15,
                         ),
                     ),
                     array(
                         'name'    => 'Digits'
                     ),
                 ),
             ));
             $this->inputFilter = $inputFilter;
         }

         return $this->inputFilter;
     }
 }
