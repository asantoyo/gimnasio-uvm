<?php
namespace Application\Form\Element;

use Zend\Form\Element\Select;

class CarrerasSelect extends Select
{

	protected $carrerasMapper;

    public function __construct($carrerasMapper = null)
    {
        $this->carrerasMapper = $carrerasMapper;
    }

	public function init()
	{
		$this->setValueOptions($this->populateCarreras());
		$this->setEmptyOption('Selecciona una carrera...');
	}

	protected function populateCarreras()
	{
		$carreras = $this->carrerasMapper->all();
		$carrerasOptions = array();
		foreach($carreras as $k=>$carrera) {
			$carrerasOptions[strval($carrera->idCarrera)] = $carrera->nombre;
		}
		return $carrerasOptions;
	}
}
