<?php
namespace Application\Form\Element;

use Zend\Form\Element\Select;

class DeportesSelect extends Select
{

	protected $deportesMapper;

    public function __construct($deportesMapper = null)
    {
        $this->deportesMapper = $deportesMapper;
    }

	public function init()
	{
		$this->setValueOptions($this->populateDeportes());
		$this->setEmptyOption('Selecciona un deporte...');
	}

	protected function populateDeportes()
	{
		$deportes = $this->deportesMapper->all();
		$deportesOptions = array();
		foreach($deportes as $k=>$deporte) {
			$deportesOptions[strval($deporte->idDeporte)] = $deporte->nombre;
		}
		return $deportesOptions;
	}
}
