<?php
namespace Application\Form;

use Zend\Form\Form;

class AlumnoForm extends Form
{
    public function init()
    {
        $this->add(array(
            'name' => 'matricula',
            'type' => 'Text',
            'options' => array(
                'label' => 'Matricula',
            ),
        ));
        $this->add(array(
            'name' => 'nombre',
            'type' => 'Text',
            'options' => array(
                'label' => 'Nombre',
            ),
        ));
        $this->add(array(
            'name' => 'apellidos',
            'type' => 'Text',
            'options' => array(
                'label' => 'Apellidos',
            ),
        ));
        $this->add(array(
            'type' => 'CarrerasSelect',
            'name' => 'idCarrera',
            'options' => array(
            	'label' => 'Carrera'
            )
	 	));
        $this->add(array(
            'type' => 'DeportesSelect',
            'name' => 'idDeporte',
            'options' => array(
                'label' => 'Deporte',
				'disable_inarray_validator' => true
            )
	 	));
        $this->add(array(
            'type' => 'Hidden',
            'name' => 'activo',
			'attributes' => array(
				'value' => '1'
			)
	 	));
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Registrar',
                'id' => 'submitbutton',
            ),
        ));

		$this->setValidationGroup(array(
			'matricula',
			'nombre',
			'apellidos',
			'idCarrera',
			'idDeporte',
			'activo'
		));
    }

}
