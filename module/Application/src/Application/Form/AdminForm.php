<?php
namespace Application\Form;

use Zend\Form\Form;

class AdminForm extends Form
{
    public function init()
    {
        $this->add(array(
            'name' => 'matricula',
            'type' => 'Text',
            'options' => array(
                'label' => 'Matricula',
            ),
        ));
		$this->add(array(
            'name' => 'nombre',
            'type' => 'Text',
            'options' => array(
                'label' => 'Nombre',
            ),
        ));
		$this->add(array(
            'name' => 'password',
            'type' => 'Password',
            'options' => array(
                'label' => 'Password',
            ),
        ));
		$this->add(array(
			'name'       => 'password2',
			'type'       => 'Password',
			'options' => array(
                'label' => 'Confirmation',
            )
		));
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Agregar',
                'id' => 'submitbutton',
            ),
        ));
    }

}
