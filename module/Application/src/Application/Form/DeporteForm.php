<?php
namespace Application\Form;

use Zend\Form\Form;

class DeporteForm extends Form
{
    public function init()
    {
        $this->add(array(
            'name' => 'nombre',
            'type' => 'Text',
            'options' => array(
                'label' => 'Deporte',
            ),
        ));
        $this->add(array(
            'name' => 'submit',
            'type' => 'Submit',
            'attributes' => array(
                'value' => 'Agregar',
                'id' => 'submitbutton',
            ),
        ));

		$this->setValidationGroup(array(
			'nombre'
		));
    }

}
