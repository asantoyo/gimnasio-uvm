<?php
/**
 * Zend Framework (http://framework.zend.com/)
 *
 * @link      http://github.com/zendframework/ZendSkeletonApplication for the canonical source repository
 * @copyright Copyright (c) 2005-2015 Zend Technologies USA Inc. (http://www.zend.com)
 * @license   http://framework.zend.com/license/new-bsd New BSD License
 */

return array(
    'router' => array(
        'routes' => array(
            'home' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Checkin',
                        'action'     => 'index',
                    ),
                ),
            ),
			'login' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/login',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Auth',
                        'action'     => 'index',
                    ),
                ),
            ),
            'logout' => array(
                'type' => 'Zend\Mvc\Router\Http\Literal',
                'options' => array(
                    'route'    => '/logout',
                    'defaults' => array(
                        'controller' => 'Application\Controller\Auth',
                        'action'     => 'logout',
                    ),
                ),
            ),
            // The following is a route to simplify getting started creating
            // new controllers and actions without needing to create a new
            // module. Simply drop new controllers in, and you can access them
            // using the path /application/:controller/:action
            'application' => array(
                'type'    => 'Literal',
                'options' => array(
                    'route'    => '/application',
                    'defaults' => array(
                        '__NAMESPACE__' => 'Application\Controller',
                        'controller'    => 'Index',
                        'action'        => 'index',
                    ),
                ),
                'may_terminate' => true,
                'child_routes' => array(
                    'default' => array(
                        'type'    => 'Segment',
                        'options' => array(
                            'route'    => '/[:controller[/:action]]',
                            'constraints' => array(
                                'controller' => '[a-zA-Z][a-zA-Z0-9_-]*',
                                'action'     => '[a-zA-Z][a-zA-Z0-9_-]*',
                            ),
                            'defaults' => array(
								'__NAMESPACE__' => 'Application\Controller',
                        		'action'        => 'index',
                            ),
                        ),
                    ),
                ),
            ),
        ),
    ),
    'service_manager' => array(
        'abstract_factories' => array(
            'Zend\Cache\Service\StorageCacheAbstractServiceFactory',
            'Zend\Log\LoggerAbstractServiceFactory',
        ),
        'aliases' => array(
            'translator' => 'MvcTranslator',
        ),
    ),
    'translator' => array(
        'locale' => 'es_ES',
        'translation_file_patterns' => array(
            array(
                'type'     => 'gettext',
                'base_dir' => __DIR__ . '/../language',
                'pattern'  => '%s.mo',
            ),
        ),
    ),
    'controllers' => array(
        'invokables' => array(
            'Application\Controller\Registro' => 'Application\Controller\RegistroController',
            'Application\Controller\Auth' => 'Application\Controller\AuthController',
            'Application\Controller\Checkin' => 'Application\Controller\CheckinController',
            'Application\Controller\Alumnos' => 'Application\Controller\AlumnosController',
            'Application\Controller\Metricas' => 'Application\Controller\MetricasController',
            'Application\Controller\Database' => 'Application\Controller\DatabaseController',
            'Application\Controller\Admin' => 'Application\Controller\AdminController',
            'Application\Controller\Import' => 'Application\Controller\ImportController',
        ),
    ),
    'view_manager' => array(
        'display_not_found_reason' => true,
        'display_exceptions'       => true,
        'doctype'                  => 'HTML5',
        'not_found_template'       => 'error/404',
        'exception_template'       => 'error/index',
        'template_map' => array(
            'layout/layout'           => __DIR__ . '/../view/layout/layout.phtml',
            'application/registro/index' => __DIR__ . '/../view/application/registro/index.phtml',
            'application/registro/correcto' => __DIR__ . '/../view/application/registro/correcto.phtml',
            'application/registro/registrar' => __DIR__ . '/../view/application/registro/index.phtml',
            'application/auth/index' => __DIR__ . '/../view/application/auth/index.phtml',
            'application/auth/login' => __DIR__ . '/../view/application/auth/index.phtml',
            'application/checkin/index' => __DIR__ . '/../view/application/checkin/index.phtml',
            'application/checkin/registro' => __DIR__ . '/../view/application/checkin/index.phtml',
            'application/checkin/correcto' => __DIR__ . '/../view/application/checkin/correcto.phtml',
            'application/alumnos/index' => __DIR__ . '/../view/application/alumnos/index.phtml',
            'application/alumnos/edit' => __DIR__ . '/../view/application/alumnos/edit.phtml',
            'application/alumnos/editsave' => __DIR__ . '/../view/application/alumnos/edit.phtml',
            'application/metricas/index' => __DIR__ . '/../view/application/metricas/index.phtml',
            'application/database/index' => __DIR__ . '/../view/application/database/index.phtml',
            'application/database/newcarrera' => __DIR__ . '/../view/application/database/newcarrera.phtml',
            'application/database/editcarrera' => __DIR__ . '/../view/application/database/editcarrera.phtml',
            'application/database/editcarrerasave' => __DIR__ . '/../view/application/database/editcarrera.phtml',
            'application/database/newdeporte' => __DIR__ . '/../view/application/database/newdeporte.phtml',
            'application/database/editdeporte' => __DIR__ . '/../view/application/database/editdeporte.phtml',
            'application/database/editdeportesave' => __DIR__ . '/../view/application/database/editdeporte.phtml',
            'application/admin/index' => __DIR__ . '/../view/application/admin/index.phtml',
            'application/admin/newadmin' => __DIR__ . '/../view/application/admin/index.phtml',
            'application/import/index' => __DIR__ . '/../view/application/import/index.phtml',
            'error/404'               => __DIR__ . '/../view/error/404.phtml',
            'error/index'             => __DIR__ . '/../view/error/index.phtml',
        ),
        'template_path_stack' => array(
            __DIR__ . '/../view',
        ),
		'strategies' => array(
            'ViewJsonStrategy',
        ),
    ),
    // Placeholder for console routes
    'console' => array(
        'router' => array(
            'routes' => array(
            ),
        ),
    ),
);
