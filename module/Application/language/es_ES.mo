��    2      �  C   <      H     I     ^  
   g     r  	   �     �     �     �  
   �     �     �     �     �            
        *  
   3     >     O     T  
   `     k     x     }     �     �     �     �     �     �  	   �  	   �     �     �     �     �     �     �                    $     9     B  2   V  M   �  .   �  9     #  @     d	     {	     �	     �	     �	     �	     �	     �	  
   
     
     "
     *
     3
     ?
     H
     ]
     i
  
   w
     �
     �
     �
     �
     �
     �
  	   �
     �
     �
             	        "     )     =     K     j     s     �     �     �     �     �     �     �     �     �     �  *     $   G  B   l                   !                 )          #          /   -   +            1   .       0   '   
                 (       *                 $                    ,   	                                 %   2                "         &       A 404 error occurred Activate Add career Add new student Add sport Additional information All rights reserved. An error occurred Attendance Attendance per hour Career Careers Certificate Check in Check in correct! Controller Database Deactivate Deactivate user? Edit Edit career Edit sport Edit student File ID Log in Log out Login Message Metrics Name New Admin New sport No Exception available Options Previous exceptions Renew Return Sign up Sign up correct! Sport Sports Student registration Students Students per career The requested URL could not be matched by routing. The requested controller could not be mapped to an existing controller class. The requested controller was not dispatchable. We cannot determine at this time why a 404 was generated. Project-Id-Version: GYM UVM
Report-Msgid-Bugs-To: 
POT-Creation-Date: 2015-05-15 22:37-0600
PO-Revision-Date: 2015-05-15 23:58-0600
Last-Translator: Evan Coury <me@evancoury.com>
Language-Team: Compas <compas@porky.com>
Language: es_MX
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
X-Poedit-KeywordsList: translate
X-Poedit-Basepath: /Users/jano/Documents/UVM/GYM/module/Application
X-Generator: Poedit 1.7.6
Plural-Forms: nplurals=2; plural=(n != 1);
X-Poedit-SearchPath-0: ..
X-Poedit-SearchPath-1: .
 Occurrió un error 404 Activar Agregar carrera Agregar nuevo estudiante Agregar deporte Información adicional Todos los derechos reservados Ocurrió un error Asistencia Asistencia por hora Carrera Carreras Certificado Registro ¡Registro correcto! Controlador Base de datos Desactivar ¿Desactivar usuario? Editor Editar carrera Editar deporte Editar estudiante Archivo Matricula Iniciar sesión Cerrar sesión Iniciar sesión Mensaje Métricas Nombre Nuevo Administrador Nuevo deporte No hay excepciones disponibles Opciones Excepciones anteriores Renovar Regresar Registro Registro correcto Deporte Deportes Registro de alumno Estudiantes Estudiantes por carrera El URL no pudo ser encontrado La clase del controlador no fue encontrada El controlador no pudo ser ejecutado No podemos determinar en este momento por qué fue generado un 404 